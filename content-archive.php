<?php the_date('F j, Y', '<p class="date">', '</p>'); ?>
<h3>
  <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title() ?></a>
</h3>
<?php echo get_the_term_list( get_the_ID(), 'category' , '<p><small>Categories: ', ', ', '</small></p>' ); ?>
<?php echo get_the_tag_list('<p><small>Tags: ',', ','</small></p>', get_the_ID()); ?>
<?php
if (get_option('show_byline_on_posts')) :
?>
<div class="author-info">
    <?php the_author(); ?>
    <p class="author-desc"> <small><?php the_author_meta(); ?></small></p>
</div>
<?php
endif;
  if ( ! is_home() && ! is_search() && ! is_archive() ) :
    uw_mobile_menu();
  endif;
 // if ( has_post_thumbnail() ) :
 // 	the_post_thumbnail( 'thumbnail' , 'style=margin-bottom:5px;');
 // endif;
   // check if the post or page has a Featured Image assigned to it.
   if ( is_home() || is_archive()  ) {
     // check if the post or page has a Featured Image assigned to it.
     if ( has_post_thumbnail() ) {
       $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
       echo '<a href="' . $large_image_url[0] . '" title="' . the_title_attribute('echo=0') . '" >';
       the_post_thumbnail();
       echo '</a>';
     }
     the_content();
     //comments_template(true);
   } else {
     the_post_thumbnail( array(130, 130), array( 'class' => 'attachment-post-thumbnail blogroll-img' ) );
     the_excerpt();
   }
?>
<hr>
